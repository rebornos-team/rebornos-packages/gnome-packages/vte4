# vte4

Virtual Terminal Emulator widget

https://wiki.gnome.org/Apps/Terminal/VTE

https://gitlab.gnome.org/GNOME/vte

**Required by gnome-console***

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/gnome-packages/vte4.git
```


